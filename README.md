# OpenAlchemist/AlchemyQuest - Development version

[[_TOC_]]

## What OpenAlchemist needs ?

OpenAlchemist is based on [SDL2, SDL2_image, SDL2_mixer](https://www.libsdl.org), [boost](https://www.boost.org/), [expat](https://libexpat.github.io/), and [libzip](https://libzip.org/).
You must install these libraries before compiling this game.

You also need the "zip" program, in order to make skins archives.

## How to compile/install it

Use GNU autotools
It's the classic chain 

```
./configure && make && sudo make install".
```

See `INSTALL` for more informations

## How to play with it

After compiling, type in the prompt: 
```
openalchemist
```

## How to contact us

The original creators are not involved with this modification. You can contact the current programmer at dulsi (at) identicalsoftware.com

You can contact the original coder at kepho.o (at) gmail.com
You can contact the original graphist at guigozz (at) gmail.com
Official website is : www.openalchemist.com

## Licensing

Code is protected by GNU GPL 2+ (see `CODE-LICENSE`)
Graphics are protected by http://creativecommons.org/licenses/by-sa/2.0/fr/

## Derivated projects

If you want to make a derivated project with OpenAlchemist code or graphs, you can do it. But a message from you to show us your project will  be appreciated (we're curious :)).

## Special Thanks

Special thanks to silkut for his win32 port and MrPouit for his ubuntu packages.
Also thanks to the ClanLib team.

