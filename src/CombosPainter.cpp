// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : CombosPainter.cpp
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#include <SDL.h>
#include "SDL_CL_ResourceManager.h"
#include "CombosPainter.h"
#include "CommonResources.h"
#include "misc.h"
#include "memory.h"
#include "GameEngine.h"

/************************************************************************/
/* Constant                                                             */
/************************************************************************/
static const float COMBOS_SPEED = 1.0f;

/************************************************************************/
/* Constructor                                                          */
/************************************************************************/
CombosPainter :: CombosPainter()
{
	_next_time = 0;
	_is_enabled = false;
	_from_right = true;
	_has_color = false;
	_state = STATE_APPEARING;
}

/************************************************************************/
/* Destructor                                                           */
/************************************************************************/
CombosPainter :: ~CombosPainter()
{
	unload_gfx();
}

/************************************************************************/
/* Load GFX                                                             */
/************************************************************************/
void CombosPainter :: load_gfx(SDL_Renderer *gc, std::string skin, std::string basename)
{

	unload_gfx();

	// Getting skins resources
	CL_ResourceManager gfx_combos("combos.xml");

	_font = CL_Font_Sprite(gc, basename + "combos/font", &gfx_combos);
	if (gfx_combos.resource_exists(basename + "combos/text/single"))
	{
		_text_single = gfx_combos.get_string_resource(basename + "combos/text/single", "");
		_text_plural = gfx_combos.get_string_resource(basename + "combos/text/plural", "");
		_plural_width = _font.get_text_width(gc, _text_plural);
	}
	else
	{
		_sprite_single = CL_Sprite(gc, basename + "combos/text/sprite_single", &gfx_combos);
		_sprite_plural = CL_Sprite(gc, basename + "combos/text/sprite_plural", &gfx_combos);
		_plural_width = _sprite_plural.get_width();
	}
	if (gfx_combos.resource_exists(basename + "combos/text/color/r"))
	{
		_has_color = true;
		_text_color.r =  CL_Integer_to_int(basename + "combos/text/color/r", &gfx_combos);
		_text_color.g =  CL_Integer_to_int(basename + "combos/text/color/g", &gfx_combos);
		_text_color.b =  CL_Integer_to_int(basename + "combos/text/color/b", &gfx_combos);
	}

	_sprite_x =  CL_Integer_to_int(basename + "combos/text/left", &gfx_combos);
	_sprite_y =  CL_Integer_to_int(basename + "combos/text/top", &gfx_combos);
	_from_right =  CL_Integer_to_int(basename + "combos/text/from_right", &gfx_combos, true);  

	_score_x =  CL_Integer_to_int(basename + "combos/score/left", &gfx_combos);
	_score_y =  CL_Integer_to_int(basename + "combos/score/top", &gfx_combos);  

	_font_height = (int)_font.get_text_height(gc, "1234567890");

}

/************************************************************************/
/* Unload GFX                                                           */
/************************************************************************/
void CombosPainter :: unload_gfx()
{

}

/************************************************************************/
/* Set score                                                            */
/************************************************************************/
void CombosPainter :: set_score(int score)
{
	this -> _score = score;

	if(!_is_enabled)
	{
		_score_current_y  = - (float)_font_height;
		if (_from_right)
			_sprite_current_x = (alchemyQuest ? ALCHEMYQUEST_WIDTH : GAME_WIDTH);
		else
			_sprite_current_x = 0 - _plural_width;
		_state = STATE_APPEARING;
	}
	else
	{
		_next_time = SDL_GetTicks() + 1500;
	}

	_is_enabled = true;

}

void CombosPainter::clear()
{
	_is_enabled = false;
}

/************************************************************************/
/* Draw                                                                 */
/************************************************************************/
void CombosPainter :: draw(SDL_Renderer *gc)
{
	if(!_is_enabled)
		return;

	if (_has_color)
		_font.draw_text(gc, _score_x, (int)_score_current_y + _font_height,
			to_string(_score), _text_color);
	else
		_font.draw_text(gc, _score_x, (int)_score_current_y + _font_height,
			to_string(_score));
	if (!_text_plural.empty())
	{
		if (_score == 1)
		{
			if (_has_color)
				_font.draw_text(gc, _sprite_current_x, (int)_sprite_y + _font_height,
					_text_single, _text_color);
			else
				_font.draw_text(gc, _sprite_current_x, (int)_sprite_y + _font_height,
					_text_single);
		}
		else
		{
			if (_has_color)
				_font.draw_text(gc, _sprite_current_x, (int)_sprite_y + _font_height,
					_text_plural, _text_color);
			else
				_font.draw_text(gc, _sprite_current_x, (int)_sprite_y + _font_height,
					_text_plural);
		}
	}
	else
	{
		if(_score == 1)
			_sprite_single.draw(gc, (int)_sprite_current_x, _sprite_y);
		else
			_sprite_plural.draw(gc, (int)_sprite_current_x, _sprite_y);
	}
}

/************************************************************************/
/* Update                                                               */
/************************************************************************/
void CombosPainter :: update()
{
	if(_is_enabled)
	{
		switch(_state)
		{
		case STATE_APPEARING:
			_update_appearing();
			break;  
		case STATE_DISPLAY:
			_update_display();
			break;
		case STATE_DISAPPEARING:
			_update_disappearing();
			break;
		}
	}
}

/************************************************************************/
/* Update appearing                                                     */
/************************************************************************/
void CombosPainter :: _update_appearing()
{
	// Getting resources
	CommonResources* p_resources = common_resources_get_instance();

	if (_from_right)
	{
		if(_sprite_current_x > _sprite_x)
		{
			_sprite_current_x -= COMBOS_SPEED * p_resources -> delta_time;
			if(_sprite_current_x < _sprite_x)
				_sprite_current_x = (float)_sprite_x;
		}
	}
	else
	{
		if(_sprite_current_x < _sprite_x)
		{
			_sprite_current_x += COMBOS_SPEED * p_resources -> delta_time;
			if(_sprite_current_x > _sprite_x)
				_sprite_current_x = (float)_sprite_x;
		}
	}

	if(_score_current_y < _score_y)
	{
		_score_current_y += COMBOS_SPEED * p_resources -> delta_time;
		if(_score_current_y > _score_y)
			_score_current_y = (float)_score_y;
	}

	if(_score_current_y == _score_y && _sprite_current_x == _sprite_x)
	{
		_state = STATE_DISPLAY;
		_next_time = SDL_GetTicks() + 1500;
	}
}

/************************************************************************/
/* Update display                                                       */
/************************************************************************/
void CombosPainter :: _update_display()
{
	if(SDL_GetTicks() > _next_time)
	{
		_state = STATE_DISAPPEARING;
	}
}

/************************************************************************/
/* Update disappearing                                                  */
/************************************************************************/
void CombosPainter :: _update_disappearing()
{
	// Getting resources
	CommonResources *resources = common_resources_get_instance();

	if (_from_right)
	{
		if(_sprite_current_x < GAME_WIDTH)
		{
			_sprite_current_x += COMBOS_SPEED * resources -> delta_time;
			if (_sprite_current_x > GAME_WIDTH)
				_sprite_current_x = GAME_WIDTH;
		}
	}
	else
	{
		if(_sprite_current_x > 0 - _plural_width)
		{
			_sprite_current_x -= COMBOS_SPEED * resources -> delta_time;
			if (_sprite_current_x < 0 - _plural_width)
				_sprite_current_x = 0 - _plural_width;
		}
	}

	if(_score_current_y > -_font_height)
	{
		_score_current_y -= COMBOS_SPEED * resources -> delta_time;
	}

	if ((_score_current_y <= - _font_height) &&
		((_from_right && _sprite_current_x >= GAME_WIDTH) ||
		(!_from_right && _sprite_current_x <= 0 - _plural_width)))
	{
		_is_enabled = false;
	}
}
