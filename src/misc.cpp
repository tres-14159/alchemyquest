// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : misc.cpp
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#include <cstdlib>
#include <sstream>
#include "misc.h"
#include "memory.h"
#include <boost/filesystem.hpp>

bool alchemyQuest(false);

/************************************************************************/
/* Get version                                                          */
/************************************************************************/
std::string get_version()
{
	return "0.4-svn";
}

/************************************************************************/
/* CL_Integer to int                                                    */
/************************************************************************/
int CL_Integer_to_int(const std::string& ressource_name, CL_ResourceManager* p_gfx, int d)
{    
	return p_gfx->get_integer_resource(ressource_name, d);
}

/************************************************************************/
/* CL_Boolean to bool                                                   */
/************************************************************************/
int CL_Boolean_to_bool(const std::string& ressource_name, CL_ResourceManager* p_gfx)
{    
	return p_gfx->get_boolean_resource(ressource_name, false);
}

/************************************************************************/
/* Get save path                                                        */
/************************************************************************/
std::string get_save_path()
{
#ifdef WIN32
#ifdef PORTABLE_APP
	if (alchemyQuest)
		return ".\\alchemyquest-savedata";
	else
		return ".\\savedata";
#else
	if(getenv("APPDATA") != NULL)
	{
		if (alchemyQuest)
			return ((std::string)getenv("APPDATA")) + "\\AlchemyQuest";
		else
			return ((std::string)getenv("APPDATA")) + "\\OpenAlchemist";
	}
	else
	{
		if (alchemyQuest)
			return ".\\alchemyquest-savedata";
		else
			return ".\\savedata";
	}
#endif
#else
	std::string save_path;
	char *data_home = getenv("XDG_DATA_HOME");
	if (data_home)
	{
		save_path = data_home;
		if (save_path[save_path.length() - 1] != '/')
		{
			save_path += "/";
		}
	}
	else
	{
		char *home = getenv("HOME");
		if (home)
		{
			save_path = home;
			if (save_path[save_path.length() - 1] != '/')
			{
				save_path += "/";
			}
			save_path += ".local/share/";
		}
	}
	if (alchemyQuest)
		save_path += "alchemyquest";
	else
		save_path += "openalchemist";
	// Create directory.
	boost::filesystem::create_directory(save_path);
	return save_path;
#endif
}

/************************************************************************/
/* Get skin path                                                        */
/************************************************************************/
std::string get_skins_path()
{
#ifdef WIN32
	std::string dir;
	if (alchemyQuest)
		dir = "./alchemyquest-skins\\";
	else
		dir = "./skins\\";
#else
#ifdef DATA_DIR
	std::string dir = DATA_DIR;
	char *data_home = getenv("XDG_DATA_DIRS");
	if (data_home)
		dir = data_home;
	if (alchemyQuest)
		dir = get_combine_path(dir, "openalchemist/alchemyquest-skins");
	else
		dir = get_combine_path(dir, "openalchemist/skins");
#else
	std::string dir = "./";
	if (alchemyQuest)
		dir += "alchemyquest-skins";
	else
		dir += "skins";
#endif
#endif

	return dir;
}

/************************************************************************/
/* Get user skin path                                                   */
/************************************************************************/
std::string get_user_skins_path()
{
	std::string dir = get_save_path();
	dir += get_path_separator() + "skins" + get_path_separator();
	return dir;	
}

/************************************************************************/
/* Get elements path                                                    */
/************************************************************************/
std::string get_elements_path()
{
#ifdef WIN32
	std::string dir;
	if (alchemyQuest)
		dir = "./alchemyquest-elements\\";
	else
		dir = "./elements\\";
#else
#ifdef DATA_DIR
	std::string dir = DATA_DIR;
	char *data_home = getenv("XDG_DATA_DIRS");
	if (data_home)
		dir = data_home;
	if (alchemyQuest)
		dir = get_combine_path(dir, "openalchemist/alchemyquest-elements");
	else
		dir = get_combine_path(dir, "openalchemist/elements");
#else
	std::string dir = "./";
	if (alchemyQuest)
		dir += "alchemyquest-elements";
	else
		dir += "elements";
#endif
#endif

	return dir;
}

/************************************************************************/
/* Get data path                                                        */
/************************************************************************/
std::string get_data_path()
{
#ifdef WIN32
	std::string file_path = "./data\\";
#else
#ifdef DATA_DIR
	std::string file_path = DATA_DIR;
	char *data_home = getenv("XDG_DATA_DIRS");
	if (data_home)
		file_path = data_home;
	file_path = get_combine_path(file_path, "openalchemist/data/");
#else
	std::string file_path = "./data/";
#endif
#endif
	return file_path;
}

/************************************************************************/
/* Get data path                                                        */
/************************************************************************/
std::string get_music_path()
{
#ifdef WIN32
	std::string dir = "./music\\";
#else
#ifdef DATA_DIR
	std::string dir = DATA_DIR;
	char *data_home = getenv("XDG_DATA_DIRS");
	if (data_home)
		dir = data_home;
	dir = get_combine_path(dir, "openalchemist/music");
#else
	std::string dir = "./";
	dir += "music";
#endif
#endif

	return dir;
}

/************************************************************************/
/* Get data path                                                        */
/************************************************************************/
std::string get_sounds_path()
{
#ifdef WIN32
	std::string dir = "./sounds\\";
#else
#ifdef DATA_DIR
	std::string dir = DATA_DIR;
	char *data_home = getenv("XDG_DATA_DIRS");
	if (data_home)
		dir = data_home;
	dir = get_combine_path(dir, "openalchemist/sounds");
#else
	std::string dir = "./";
	dir += "sounds";
#endif
#endif

	return dir;
}

std::string get_combine_path(std::string dirs, std::string subdir)
{
	std::vector<std::string> dirs_split;
	std::stringstream ss(dirs);
	std::string item;
	while (std::getline(ss, item, ':'))
	{
		if (item[item.length() - 1] == '/')
			dirs_split.push_back(item);
		else
			dirs_split.push_back(item + "/");
	}
	for (int i = 0; i < dirs_split.size(); i++)
	{
		if (boost::filesystem::exists(dirs_split[i] + subdir))
			return dirs_split[i] + subdir;
	}
	return "";
}

/************************************************************************/
/* Get path separator                                                   */
/************************************************************************/
std::string get_path_separator()
{
#ifdef WIN32
	return "\\";
#else
	return "/";
#endif
}
