// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : Window.cpp
//  Description : Window used to display game
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or +
//
// **********************************************************************

#include "Window.h"
#include "CommonResources.h"
#include "Preferences.h"
#include "GameEngine.h"
#include "misc.h"
#include <SDL_image.h>

/************************************************************************/
/* Constructor                                                          */
/************************************************************************/
GameWindow::GameWindow(int game_width, int game_height)
{
	_game_width = game_width;
	_game_height = game_height;
	_scale = 1;
	_game_window = NULL;
	_game_renderer = NULL;
	_dx = 0;
	_dy = 0;
}

/************************************************************************/
/* Destructor                                                           */
/************************************************************************/
GameWindow::~GameWindow()
{
}

/************************************************************************/
/* Min                                                                  */
/************************************************************************/
#ifndef WIN32
inline float min(float a, float b){ return a < b ? a : b; }
#endif

/************************************************************************/
/* Manager                                                              */
/************************************************************************/
void GameWindow::manage(GameEngine& game_engine)
{
	bool reload_skin = false;
	if (_game_renderer)
	{
	   SDL_DestroyRenderer(_game_renderer);
	   _game_renderer = NULL;
	   SDL_DestroyWindow(_game_window);
	   _game_window = NULL;
	   reload_skin = true;
	}

	// Window description
	Preferences* p_pref = pref_get_instance();

	// Open the window
	_scale = p_pref->scale;
	_game_window = SDL_CreateWindow((alchemyQuest ? "AlchemyQuest" : "OpenAlchemist"),
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		_game_width * _scale,
		_game_height * _scale,
		(p_pref -> fullscreen ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0) | (alchemyQuest ? 0 : SDL_WINDOW_RESIZABLE));
	if (_game_window == NULL)
	{
		std::cout << "Failed - SDL_CreateWindow" << std::endl;
		exit(0);
	}

	_game_renderer = SDL_CreateRenderer(_game_window, -1, ((p_pref->render_target == Preferences::SOFTWARE) ? SDL_RENDERER_SOFTWARE : 0));
	if (_game_renderer == NULL)
	{
		std::cout << "Failed - SDL_CreateRenderer" << std::endl;
		exit(0);
	}
	SDL_RenderSetLogicalSize(_game_renderer, _game_width, _game_height);
	if (p_pref->fullscreen)
	{
		int width, height;
		SDL_GetWindowSize(_game_window, &width, &height);
		_scale = height * 1.0 / _game_height;
		if (width * 1.0 /_game_width < _scale)
		{
			_scale = height * 1.0 / _game_height;
			_dy = (height - (_game_height * _scale)) / 2;
		}
		else
		{
			_dx = (width - (_game_width * _scale)) / 2;
		}
	}
	else
	{
		_dx = 0;
		_dy = 0;
	}

	// Add an icon
	SDL_Surface *icon = IMG_Load((get_data_path()+"logo.png").c_str());
	SDL_SetWindowIcon(_game_window, icon);
	SDL_FreeSurface(icon);

	// Drawing black window
	prepare();
	display();

	// Set some variables
	CommonResources* p_common_resources = common_resources_get_instance();
	p_common_resources -> p_gc = get_gc();

	if (reload_skin)
		game_engine.set_skin(p_pref -> skin);
}

/************************************************************************/
/* Clear                                                                */
/************************************************************************/
void GameWindow::prepare()
{
}

/************************************************************************/
/* Display                                                              */
/************************************************************************/
void GameWindow::display()
{
	SDL_RenderPresent(_game_renderer);
}
