// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : misc.h
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#ifndef _MISC_H_
#define _MISC_H_

#include <SDL.h>
#include "SDL_CL_ResourceManager.h"
#include <string>
#include <sstream>  
#include <iostream>

extern bool alchemyQuest;

/** This function return the current version name */
std::string get_version();

/** This function return a factor to compute move with time */
inline float get_time_interval(int fps)
{
	static unsigned int last_time = 0;
	unsigned int delta_time = SDL_GetTicks() - last_time;
	last_time += delta_time;

	if(fps == 0)
	{
		return (float)delta_time;
	}

	float fps_normal_time = 1000.0f/((float)fps);
	if(delta_time > fps_normal_time * 1.1 || delta_time < fps_normal_time * 0.9)
	{
		return (float)delta_time;
	}	

	return fps_normal_time;
}

/** Cast everything to string */
template<typename T>
inline std::string to_string( const T & Value )
{

	std::ostringstream oss;

	oss << Value;

	return oss.str();
}

/** Format number like 000.000.000 */
inline std::string format_number(const std::string& string)
{
	int length = (int)string.length();
	std::string string_modified = "";
	for(int i = 0; i<length; ++i)
	{
		if((length - i) % 3 == 0 && i>0)
		{
			string_modified += ".";
		}
		string_modified += string.substr(i,1);
	}
	return string_modified;
}

/** Returning save path */
std::string get_save_path();

/** Returning skins path */
std::string get_skins_path();

/** Returning user skins path */
std::string get_user_skins_path();

/** Returning elements path */
std::string get_elements_path();

/** Returning data path */
std::string get_data_path();

/** Returning music path */
std::string get_music_path();

/** Returning sounds path */
std::string get_sounds_path();

std::string get_combine_path(std::string dirs, std::string subdir);

/** Returning path separator ("/" or "\\" - thx to Windows.) */
std::string get_path_separator();

/** Convert a CL_Integer to a int */
int CL_Integer_to_int(const std::string& ressource_name, CL_ResourceManager* p_gfx, int d = 0);

/** Convert a CL_Boolean to a boolean */
int CL_Boolean_to_bool(const std::string& ressource_name, CL_ResourceManager* p_gfx);

#endif
