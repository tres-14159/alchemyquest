// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : Keys.cpp
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#include <SDL.h>

#include "Keys.h"
#include "memory.h"

/************************************************************************/
/* Constructor                                                          */
/************************************************************************/
Keys::Keys():
fullscreen(SDL_SCANCODE_F11,    false),
left      (SDL_SCANCODE_LEFT,   true ),
right     (SDL_SCANCODE_RIGHT,  true ),
retry     (SDL_SCANCODE_F2,     false),
undo      (SDL_SCANCODE_F5,     false),
escape    (SDL_SCANCODE_ESCAPE, false),
pause     (SDL_SCANCODE_PAUSE,  false),
up        (SDL_SCANCODE_UP,     true ),
down      (SDL_SCANCODE_DOWN,   true ),
enter     (SDL_SCANCODE_RETURN, false),
options   (SDL_SCANCODE_F9,     false),
skins     (SDL_SCANCODE_F10,    false),
framerate (SDL_SCANCODE_F,      false),
space     (SDL_SCANCODE_SPACE,  false),
q         (SDL_SCANCODE_Q,      false),
num1      (SDL_SCANCODE_1,      false),
num2      (SDL_SCANCODE_2,      false)
{

}

/************************************************************************/
/* Destructor                                                           */
/************************************************************************/
Keys::~Keys()
{
}
