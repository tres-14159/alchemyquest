// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : OptionsMenuState.h
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#ifndef _OPTIONSMENU_STATE_H_
#define _OPTIONSMENU_STATE_H_

#include <SDL.h>

#include "MenuState.h"
#include "MenuState/BasicItem.h"
#include "MenuState/MultipleChoicesItem.h"
#include "MenuState/MultipleTextChoicesItem.h"

class GameEngine;

/**
* OptionsMenu State
*/
class OptionsMenuState : public MenuState
{

public:

	/** Constructor */
	OptionsMenuState();

	/** Destructor */
	~OptionsMenuState();

	virtual void init();

	virtual void term();

	virtual void load_gfx(SDL_Renderer *gc, std::string skin);

	virtual void unload_gfx();
	
	virtual void action_performed(int selection, ActionType action_type);

	virtual void update_child();
	
	/** Toggle screen */
	void toggle_screen();

private:
};

#endif
