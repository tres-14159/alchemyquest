// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : PauseMenuState.cpp
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#include "../memory.h"
#include <SDL.h>

#include "PauseMenuState.h"
#include "../CommonResources.h"
#include "../GameEngine.h"
#include "../misc.h"

/************************************************************************/
/* Constructor                                                          */
/************************************************************************/
PauseMenuState::PauseMenuState ()
{
}

/************************************************************************/
/* Destructor                                                           */
/************************************************************************/
PauseMenuState::~PauseMenuState ()
{
	term ();
}

/************************************************************************/
/* Init                                                                 */
/************************************************************************/
void PauseMenuState::init ()
{
}

/************************************************************************/
/* Term                                                                 */
/************************************************************************/
void PauseMenuState::term ()
{ 
	unload_gfx();
}

/************************************************************************/
/* Load GFX                                                             */
/************************************************************************/
void PauseMenuState::load_gfx (SDL_Renderer *gc, std::string skin)
{
	unload_gfx();

	// Getting skins resources
	CL_ResourceManager gfx("menu_pause.xml");


	// First, the sprites
	_background = CL_Sprite (gc, "menu_pause/background", &gfx);
	_bgLabel = gfx.get_string_resource("menu_pause/label", "");

	int x = ((alchemyQuest ? ALCHEMYQUEST_WIDTH : GAME_WIDTH) / 2) - _background.get_width () / 2;
	int y = ((alchemyQuest ? ALCHEMYQUEST_HEIGHT : GAME_HEIGHT) / 2) - _background.get_height () / 2;
	if (x < 0)
		x = 0;
	if (y < 0)
		y = 0;


	// resume
	load_basic_menu_item(gc, gfx, "menu_pause/resume/", "stop_state", x, y);

	// undo
	load_basic_menu_item(gc, gfx, "menu_pause/undo/", "undo", x, y);

	// retry
	load_basic_menu_item(gc, gfx, "menu_pause/retry/", "retry", x, y);

	// options
	load_basic_menu_item(gc, gfx, "menu_pause/options/", "options", x, y);

	// skins
	load_basic_menu_item(gc, gfx, "menu_pause/changeskin/", "change_skin", x, y);

	// give up
	load_basic_menu_item(gc, gfx, "menu_pause/giveup/", "giveup", x, y);

	// quit
	load_basic_menu_item(gc, gfx, "menu_pause/quit/", "quit", x, y);
}

/************************************************************************/
/* Unload GFX                                                           */
/************************************************************************/
void PauseMenuState::unload_gfx ()
{
	_items.clear();
}

/************************************************************************/
/* Action performed                                                     */
/************************************************************************/
void PauseMenuState::action_performed (int selection, ActionType action_type)
{
	if(ACTION_TYPE_ENTER == action_type)
	{
		switch (_items[selection]->get_action())
		{
		case MenuItem::ACTION_STOP_STATE:
			_p_common_resources -> p_engine -> stop_current_state ();
			break;
		case MenuItem::ACTION_UNDO:
			_p_common_resources -> player1.undo ();
			_p_common_resources -> p_engine -> stop_current_state ();
			_p_common_resources -> p_engine -> set_state_ingame ();
			break;
		case MenuItem::ACTION_RETRY:
			{
				if ((_p_common_resources -> player1.is_game_over ()) && ((!_p_common_resources->player2.get()) || (_p_common_resources -> player2->is_game_over ())))
				{
					_p_common_resources -> player1.new_game ();
					if (_p_common_resources->player2.get())
						_p_common_resources->player2->new_game();
					_p_common_resources -> p_engine -> stop_current_state ();
					_p_common_resources -> p_engine -> set_state_ingame ();
				}
				else
				{
					_p_common_resources -> p_engine -> stop_current_state ();
					_p_common_resources -> p_engine -> set_state_quit_menu (QUITMENU_RETRY);
				}
				break;
			}
		case MenuItem::ACTION_GIVEUP:
			{
				if ((_p_common_resources -> player1.is_game_over ()) && ((!_p_common_resources->player2.get()) || (_p_common_resources -> player2->is_game_over ())))
				{
					_p_common_resources -> player2 = NULL;
					_p_common_resources -> p_engine -> stop_current_state ();
					_p_common_resources -> p_engine -> set_state_title ();
				}
				else
				{
					_p_common_resources -> p_engine -> stop_current_state ();
					_p_common_resources -> p_engine -> set_state_quit_menu (QUITMENU_GIVE_UP);
				}
				break;
			}
		case MenuItem::ACTION_OPTIONS:
			{
				start ();
				_p_common_resources -> p_engine -> set_state_options_menu ();
				break;
			}
		case MenuItem::ACTION_CHANGESKIN:
			{
				start ();
				_p_common_resources -> p_engine -> set_state_skin_menu();
				break;
			}
		case MenuItem::ACTION_QUIT:
			{
				_p_common_resources -> p_engine -> set_state_quit_menu (QUITMENU_EXIT);
				break;
			}
		}
	}

}

/************************************************************************/
/* Update child                                                         */
/************************************************************************/
void PauseMenuState::update_child ()
{
	for (auto itr : _items)
	{
		switch (itr->get_action())
		{
			case MenuItem::ACTION_UNDO:
				itr->set_locked (!_p_common_resources -> player1.is_undo_available ());
				break;
			case MenuItem::ACTION_GIVEUP:
				itr->set_locked(!_p_common_resources -> p_current_player -> is_human());
				break;
			default:
				break;
		}
	}
}
