// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : QuitMenuState.cpp
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#include "../memory.h"
#include <iostream>
#include "QuitMenuState.h"
#include "../CommonResources.h"
#include "../GameEngine.h"
#include "../misc.h"

/************************************************************************/
/* Constructor                                                          */
/************************************************************************/
QuitMenuState::QuitMenuState()
	: _choice_item(NULL)
{
}

/************************************************************************/
/* Destructor                                                           */
/************************************************************************/
QuitMenuState::~QuitMenuState()
{
	unload_gfx();
}

/************************************************************************/
/* Init                                                                 */
/************************************************************************/
void QuitMenuState::init()
{
}

/************************************************************************/
/* Term                                                                 */
/************************************************************************/
void QuitMenuState::term()
{
}

/************************************************************************/
/* Load GFX                                                             */
/************************************************************************/
void QuitMenuState::load_gfx(SDL_Renderer *gc, std::string skin)
{
	unload_gfx();

	// Getting skins resources
	CL_ResourceManager gfx("menu_quit.xml");

	_panel_exit    = CL_Sprite(gc, "menu_quit/dialog_panel/sprite_exit", &gfx);
	_panel_give_up = CL_Sprite(gc, "menu_quit/dialog_panel/sprite_giveup", &gfx);
	_panel_retry   = CL_Sprite(gc, "menu_quit/dialog_panel/sprite_retry", &gfx);

	_label_exit = gfx.get_string_resource("menu_quit/dialog_panel/label_exit", "");
	_label_give_up = gfx.get_string_resource("menu_quit/dialog_panel/label_giveup", "");
	_label_retry = gfx.get_string_resource("menu_quit/dialog_panel/label_retry", "");

	_panel_x = CL_Integer_to_int("menu_quit/dialog_panel/left", &gfx);
	_panel_y = CL_Integer_to_int("menu_quit/dialog_panel/top", &gfx);

	_choice_item = load_dual_choice_item(gc, gfx, "menu_quit/dialog_yes/", "menu_quit/dialog_no/");
}

/************************************************************************/
/* Unload GFX                                                           */
/************************************************************************/
void QuitMenuState::unload_gfx()
{
	_items.clear();
}

/************************************************************************/
/* Action performed                                                     */
/************************************************************************/
void QuitMenuState::action_performed(int selection, ActionType action_type)
{
	if(ACTION_TYPE_ENTER == action_type)
	{
		int s = _choice_item->get_current_choice();
		if(CHOICE_LEFT == s)
		{
			switch(_action)
			{
			case QUITMENU_EXIT:
				_p_common_resources -> p_engine -> stop();
				break;
			case QUITMENU_GIVE_UP:
				_p_common_resources -> p_engine -> stop_current_state();
				_p_common_resources -> p_engine -> set_state_title();
				_p_common_resources->player1.load_gfx(_p_common_resources->p_gc, _p_common_resources->skin, "player1/");
				_p_common_resources -> player2 = NULL;
				break;
			case QUITMENU_RETRY:
				_p_common_resources->player1.load_gfx(_p_common_resources->p_gc, _p_common_resources->skin, "player1/");
				_p_common_resources -> player1.new_game();
				_p_common_resources -> p_engine -> stop_current_state();
				_p_common_resources -> player2 = NULL;
				break;
			}
		}
		else
		{
			_p_common_resources -> p_engine -> stop_current_state();
		}
	}

}

/************************************************************************/
/* Update child                                                         */
/************************************************************************/
void QuitMenuState::update_child()
{
}

/************************************************************************/
/* Set action                                                           */
/************************************************************************/
void QuitMenuState::set_action(QuitMenuAction a)
{
	_action = a;
	switch(_action)
	{
	case QUITMENU_EXIT:
		_background = _panel_exit;
		_bgLabel = _label_exit;
		break;
	case QUITMENU_GIVE_UP:
		_background = _panel_give_up;
		_bgLabel = _label_give_up;
		break;
	case QUITMENU_RETRY:
		_background = _panel_retry;
		_bgLabel = _label_retry;
		break;
	}
}
