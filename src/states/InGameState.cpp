// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : InGameState.cpp
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#include "../memory.h"
#include "InGameState.h"
#include "../CommonResources.h"
#include "../GameEngine.h"
#include "../misc.h"
#include "../Window.h"
#include <SDL_mixer.h>

/************************************************************************/
/* Constructor                                                          */
/************************************************************************/
InGameState::InGameState()
{

}

/************************************************************************/
/* Destructor                                                           */
/************************************************************************/
InGameState::~InGameState()
{

}

/************************************************************************/
/* Init                                                                 */
/************************************************************************/
void InGameState::init()
{
}

/************************************************************************/
/* Term                                                                 */
/************************************************************************/
void InGameState::term()
{
}

/************************************************************************/
/* Load GFX                                                             */
/************************************************************************/
void InGameState::load_gfx(SDL_Renderer *gc, std::string skin)
{

}

/************************************************************************/
/* Unload GFX                                                           */
/************************************************************************/
void InGameState::unload_gfx()
{

}

/************************************************************************/
/* Draw                                                                 */
/************************************************************************/
void InGameState::draw(SDL_Renderer *gc)
{

}

/************************************************************************/
/* Update                                                               */
/************************************************************************/
void InGameState::update(SDL_Renderer *gc)
{
	bool game_over = true;
	_p_common_resources -> player1.update();
	if (_p_common_resources -> player1.get_game_mode() != GAME_MODE_GAME_OVER)
		game_over = false;
	if (_p_common_resources->player2.get())
	{
		_p_common_resources -> player2->update();
		if (_p_common_resources -> player2->get_game_mode() != GAME_MODE_GAME_OVER)
			game_over = false;
	}
	if (game_over)
	{
		if(_p_common_resources -> player1.get_score() > _p_common_resources -> highscore)
		{
			_p_common_resources -> p_engine -> set_state_gameover(true);
			_p_common_resources -> old_highscore = _p_common_resources -> highscore;
			_p_common_resources -> highscore = _p_common_resources -> player1.get_score();
			_p_common_resources -> save_scores();
		}
		else
		{
			_p_common_resources -> p_engine -> set_state_gameover(false);
		}
	}
}

/************************************************************************/
/* Events                                                               */
/************************************************************************/
void InGameState::events(GameWindow& window)
{
	_p_common_resources -> player1.events();
	if (_p_common_resources->player2.get())
		_p_common_resources->player2->events();

	if(_p_common_resources -> key.escape.get() || _p_common_resources ->key.pause.get())
	{
		_p_common_resources -> p_engine -> set_state_pause_menu();
	}

	if(_p_common_resources->key.skins.get())
	{
		_p_common_resources -> p_engine -> set_state_skin_menu();
	}

	if(_p_common_resources->key.options.get() )
	{
		_p_common_resources -> p_engine -> set_state_options_menu();
	}

}

/************************************************************************/
/* Front layer behind                                                   */
/************************************************************************/
bool InGameState::front_layer_behind()
{
	return false;
}
