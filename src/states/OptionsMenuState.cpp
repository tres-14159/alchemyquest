// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : OptionsMenuState.h
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#include "../memory.h"
#include <SDL.h>

#include "OptionsMenuState.h"
#include "../CommonResources.h"
#include "../GameEngine.h"
#include "../misc.h"
#include "../Preferences.h"
#include "../AudioManager.h"

/************************************************************************/
/* Render choices                                                       */
/************************************************************************/
enum{
	RENDER_CHOICE_HARDWARE = 0,
	RENDER_CHOICE_SOFTWARE
};

/************************************************************************/
/* No/Yes                                                               */
/************************************************************************/
enum{
	ITEM_NO = 0,
	ITEM_YES
};

/************************************************************************/
/* Constructor                                                          */
/************************************************************************/
OptionsMenuState::OptionsMenuState()
{
}

/************************************************************************/
/* Destructor                                                           */
/************************************************************************/
OptionsMenuState::~OptionsMenuState()
{
	term();
}

/************************************************************************/
/* Init                                                                 */
/************************************************************************/
void OptionsMenuState::init()
{
}

/************************************************************************/
/* Term                                                                 */
/************************************************************************/
void OptionsMenuState::term()
{
	unload_gfx();
}

/************************************************************************/
/* Load GFX                                                             */
/************************************************************************/
void OptionsMenuState::load_gfx(SDL_Renderer *gc, std::string skin)
{
	unload_gfx();

	// Getting skins resources
	CL_ResourceManager gfx("menu_options.xml");

	// First, the sprites
	_background = CL_Sprite(gc, "menu_options/dialog_background", &gfx);
	_bgLabel = gfx.get_string_resource("menu_options/label", "");


	int x = ((alchemyQuest ? ALCHEMYQUEST_WIDTH : GAME_WIDTH) / 2) - _background.get_width () / 2;
	int y = ((alchemyQuest ? ALCHEMYQUEST_HEIGHT : GAME_HEIGHT) / 2) - _background.get_height () / 2;
	if (x < 0)
		x = 0;
	if (y < 0)
		y = 0;

	ChoiceItem *render_item = load_basic_multiple_choices_item(gc, gfx, 
		"menu_options/render/", "render", "menu_options/render-choices/",
		"menu_options/render-choices/", x, y);
	if (render_item->get_number_choices() == 0)
	{
		MultipleChoicesItem *rd_item = static_cast<MultipleChoicesItem *>(render_item);
		rd_item->add_choice(gc, gfx, "menu_options/render-choices/opengl");
		rd_item->add_choice(gc, gfx, "menu_options/render-choices/software");
	}

	ChoiceItem *fullscreen_item = load_basic_multiple_choices_item(gc, gfx, 
		"menu_options/fullscreen/", "fullscreen", "menu_options/fullscreen-choices/",
		"menu_options/yesno/", x, y);
	if (fullscreen_item->get_number_choices() == 0)
	{
		MultipleChoicesItem *fs_item = static_cast<MultipleChoicesItem *>(fullscreen_item);
		fs_item->add_choice(gc,  gfx, "menu_options/item-no");
		fs_item->add_choice(gc,  gfx, "menu_options/item-yes");
	}

	ChoiceItem *framerate_item = load_basic_multiple_choices_item(gc, gfx, 
		"menu_options/framerate/", "framerate", "menu_options/framerate-choices/",
		"menu_options/framerate-choices/", x, y);
	if (framerate_item->get_number_choices() == 0)
	{
		MultipleChoicesItem *fr_item = static_cast<MultipleChoicesItem *>(framerate_item);
		fr_item->add_choice(gc, gfx, "menu_options/framerate-choices/30");
		fr_item->add_choice(gc, gfx, "menu_options/framerate-choices/40");
		fr_item->add_choice(gc, gfx, "menu_options/framerate-choices/50");
		fr_item->add_choice(gc, gfx, "menu_options/framerate-choices/60");
		fr_item->add_choice(gc, gfx, "menu_options/framerate-choices/80");
		fr_item->add_choice(gc, gfx, "menu_options/framerate-choices/100");
		fr_item->add_choice(gc, gfx, "menu_options/framerate-choices/no-limit");
	}

	ChoiceItem *colorblind_item = load_basic_multiple_choices_item(gc, gfx, 
		"menu_options/colorblind/", "colorblind", "menu_options/colorblind-choices/",
		"menu_options/yesno/", x, y);
	if (colorblind_item->get_number_choices() == 0)
	{
		MultipleChoicesItem *cb_item = static_cast<MultipleChoicesItem *>(colorblind_item);
		cb_item->add_choice(gc, gfx, "menu_options/item-no");
		cb_item->add_choice(gc, gfx, "menu_options/item-yes");
	}

	ChoiceItem *sound_level_item = load_basic_multiple_choices_item(gc, gfx, 
		"menu_options/sound/", "sound", "menu_options/sound-choices/",
		"menu_options/sound_level/", x, y);

	ChoiceItem *music_level_item = load_basic_multiple_choices_item(gc, gfx, 
		"menu_options/music/", "music", "menu_options/music-choices/",
		"menu_options/sound_level/", x, y);

	load_basic_menu_item(gc, gfx, "menu_options/quit/", "stop_state", x, y);

	Preferences *p_pref = pref_get_instance();		
	switch(p_pref->render_target)
	{
		case Preferences::HARDWARE:
			render_item->set_current_choice(RENDER_CHOICE_HARDWARE);
			break;
		case Preferences::SOFTWARE:
			render_item->set_current_choice(RENDER_CHOICE_SOFTWARE);
			break;
	}

	if(p_pref -> fullscreen)
	{
		fullscreen_item->set_current_choice(ITEM_YES);
	}
	else
	{
		fullscreen_item->set_current_choice(ITEM_NO);
	}
	if(p_pref -> colorblind)
	{
		colorblind_item->set_current_choice(ITEM_YES);
	}
	else
	{
		colorblind_item->set_current_choice(ITEM_NO);
	}
	if(p_pref -> maxfps <= 30)
	{
		framerate_item->set_current_choice(0);
	}
	else if(p_pref -> maxfps <= 40)
	{
		framerate_item->set_current_choice(1);
	}
	else if(p_pref -> maxfps <= 50)
	{
		framerate_item->set_current_choice(2);
	}
	else if(p_pref -> maxfps <= 60)
	{
		framerate_item->set_current_choice(3);
	}
	else if(p_pref -> maxfps <= 80)
	{
		framerate_item->set_current_choice(4);
	}
	else if(p_pref -> maxfps <= 100)
	{
		framerate_item->set_current_choice(5);
	}
	else
	{
		framerate_item->set_current_choice(6);
	}

	sound_level_item->set_current_choice(p_pref -> sound_level / 10);
	music_level_item->set_current_choice(p_pref -> music_level / 10);
}

/************************************************************************/
/* Unload GFX                                                           */
/************************************************************************/
void OptionsMenuState::unload_gfx()
{
	_items.clear();
}

/************************************************************************/
/* Action performed                                                     */
/************************************************************************/
void OptionsMenuState::action_performed(int selection, ActionType action_type)
{
	if(ACTION_TYPE_ENTER == action_type)				
	{
		switch(_items[selection]->get_action())
		{					
		case MenuItem::ACTION_STOP_STATE:
			{							
				_p_common_resources -> p_engine -> stop_current_state ();
			}
			break;
		default:
			break;
		}					
	}
}

/************************************************************************/
/* Update child                                                         */
/************************************************************************/
void OptionsMenuState::update_child()
{
	Preferences* p_pref = pref_get_instance();
	for (auto itr : _items)
	{
		switch (itr->get_action())
		{
			case MenuItem::ACTION_RENDER:
				switch(static_cast<ChoiceItem*>(itr.get())->get_current_choice())
				{
				case RENDER_CHOICE_SOFTWARE:
					if(p_pref->render_target != Preferences::SOFTWARE)
					{		
						p_pref->render_target = Preferences::SOFTWARE;
						p_pref->write();
					}
					break;
				case RENDER_CHOICE_HARDWARE:
					if(p_pref->render_target != Preferences::HARDWARE)
					{
						p_pref->render_target = Preferences::HARDWARE;
						p_pref->write();
					}
					break;
				}
				break;
			case MenuItem::ACTION_FULLSCREEN:
			{
				bool fullscreen = (int) static_cast<ChoiceItem*>(itr.get())->get_current_choice() == ITEM_YES;
				if(p_pref -> fullscreen != fullscreen)
				{
					_p_common_resources->p_engine->toggle_screen();
				}
				break;
			}
			case MenuItem::ACTION_COLORBLIND:
			{
				bool colorblind = (int) static_cast<ChoiceItem*>(itr.get())->get_current_choice() == ITEM_YES;
				if(p_pref->colorblind != colorblind)
				{
					_p_common_resources->p_engine->toggle_colorblind();
				}
				break;
			}
			case MenuItem::ACTION_SOUND:
				if((int)static_cast<ChoiceItem*>(itr.get())->get_current_choice() != p_pref -> sound_level / 10)
				{
					p_pref -> sound_level = (int) static_cast<ChoiceItem*>(itr.get())->get_current_choice() * 10;
					g_audio_manager.set_sounds_volume(p_pref -> sound_level);
					p_pref -> write();
				}
				break;
			case MenuItem::ACTION_MUSIC:
				if((int)static_cast<ChoiceItem*>(itr.get())->get_current_choice() != p_pref -> music_level / 10)
				{
					p_pref -> music_level = (int) static_cast<ChoiceItem*>(itr.get())->get_current_choice() * 10;
					g_audio_manager.set_music_volume(p_pref -> music_level);
					p_pref -> write();
				}
				break;
			case MenuItem::ACTION_FRAMERATE:
			{
				// Framerate limit
				int maxfps = 60;
				switch(static_cast<ChoiceItem*>(itr.get())->get_current_choice())
				{
				case 0:
					maxfps = 30;
					break;
				case 1:
					maxfps = 40;
					break;
				case 2:
					maxfps = 50;
					break;
				case 3:
					maxfps = 60;
					break;
				case 4:
					maxfps = 80;
					break;
				case 5:
					maxfps = 100;
					break;
				case 6:
					maxfps = 1000;
					break;
				}
				if(maxfps != p_pref->maxfps)
				{
					p_pref->maxfps = maxfps;
					_p_common_resources -> p_engine -> refresh_framerate_limit();
				}
				break;
			}
			default:
				break;
		}
	}
}

/************************************************************************/
/* Toggle screen                                                        */
/************************************************************************/
void OptionsMenuState::toggle_screen()
{
	Preferences* p_pref = pref_get_instance();		
	for (auto itr : _items)
	{
		switch (itr->get_action())
		{
			case MenuItem::ACTION_FULLSCREEN:
				if(p_pref -> fullscreen)
				{
					static_cast<ChoiceItem*>(itr.get())->set_current_choice(ITEM_YES);
				}
				else
				{
					static_cast<ChoiceItem*>(itr.get())->set_current_choice(ITEM_NO);
				}
				break;
			default:
				break;
		}
	}
}
