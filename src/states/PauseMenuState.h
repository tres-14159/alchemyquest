// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : PauseMenuState.h
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#ifndef _PAUSEMENU_STATE_H_
#define _PAUSEMENU_STATE_H_

#include "MenuState.h"
#include "MenuState/BasicItem.h"
#include "MenuState/BasicTextItem.h"
#include "../KeyboardKey.h"

/**
* PauseMenu State
*/
class PauseMenuState : public MenuState{

public:

	/** Constructor */
	PauseMenuState();

	/** Destructor */
	~PauseMenuState();

	virtual void init();

	virtual void term();

	virtual void load_gfx(SDL_Renderer *gc, std::string skin);

	virtual void unload_gfx();

	virtual void action_performed(int selection, ActionType action_type);

	virtual void update_child();	

private:
};

#endif
