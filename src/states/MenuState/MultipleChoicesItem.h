// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : MultipleChoiceItem.h
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#ifndef _MULTIPLE_CHOICES_ITEM_H_
#define _MULTIPLE_CHOICES_ITEM_H_

#include <vector>
#include <SDL.h>
#include "../../SDL_CL_Sprite.h"

#include "ChoiceItem.h"

/**
* Multiples choices item
*/
class MultipleChoicesItem : public ChoiceItem{

public:

	/** Constructor */
	MultipleChoicesItem();

	/** Destructor */
	~MultipleChoicesItem();	

	virtual void draw(SDL_Renderer *gc);

	virtual void action_performed(ActionType action_type);

	virtual bool quit_menu_on_action(){return false;}

	virtual bool is_inside(int x, int y);

	virtual void mouse_moved(int mouse_x, int mouse_y);

	/** Set choices X coord */
	void set_choice_x(int x){_choice_x = x;}

	/** Set choices Y coord */
	void set_choice_y(int y){_choice_y = y;}

	/** Set sprites for item description (not choice) */
	void set_description_sprites(SDL_Renderer *gc, CL_ResourceManager& gfx,
		std::string normal, std::string selected, std::string locked = "");

	/** Add a choice */
	void add_choice(SDL_Renderer *gc, CL_ResourceManager& gfx, std::string name);

	/** Return number of choices */
	unsigned int get_number_choices();

	/** Clear choices */
	void clear_choices();

	/** Unload GFX */
	void unload_gfx();

protected:

	/** Choices list */
	std::vector<CL_Sprite> _choices_list;

	/** Choices X coord */
	int _choice_x;

	/** Choices Y coord */
	int _choice_y;

	/** Default description sprite  */
	CL_Sprite _description_normal;

	/** Description sprite when the item is selected in the menu */
	CL_Sprite _description_selected;

	/** Description sprite when the item is locked */
	CL_Sprite _description_locked;

};



#endif
