// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : ChoiceItem.cpp
//  Description : 
//  Author      : Dennis Payne <dulsi@identicalsoftware.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#include <iostream>
#include "ChoiceItem.h"
#include "../../memory.h"

/************************************************************************/
/* Constructor                                                          */
/************************************************************************/
ChoiceItem::ChoiceItem()
{
	_is_selected = false;
	_is_locked = false;
	_alpha = 1.0f;
}

/************************************************************************/
/* Destructor                                                          */
/************************************************************************/
ChoiceItem::~ChoiceItem()
{
}

/************************************************************************/
/* Set current choice                                                   */
/************************************************************************/
void ChoiceItem::set_current_choice(unsigned int choice)
{
	if(choice < get_number_choices())
	{
		_selection = choice;
	}
}
