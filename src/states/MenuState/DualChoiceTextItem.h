// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : DualChoiceTextItem.h
//  Description : 
//  Author      : Dennis Payne <dulsi@identicalsoftware.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#ifndef _DUAL_CHOICE_TEXT_ITEM_H_
#define _DUAL_CHOICE_TEXT_ITEM_H_

#include <SDL.h>

#include "DualChoiceItem.h"

/**
* Dual Choice Item class
*/
class DualChoiceTextItem : public ChoiceItem
{

public:

	/** Constructor */
	DualChoiceTextItem();

	/** Destructor */
	~DualChoiceTextItem();

	/** Set left text */
	void set_text(std::string left_text, std::string right_text);

	/** Unload GFX */
	void unload_gfx();

	/** Set choice 2 x coord */
	void set_x2(int x);

	/** Set choice 2 y coord */
	void set_y2(int y);

	virtual void draw(SDL_Renderer *gc);
	
	virtual void action_performed(ActionType action_type);

	virtual bool quit_menu_on_action(){ return true; }

	virtual bool is_inside(int x, int y);

	virtual void mouse_moved(int mouse_x, int mouse_y);

	virtual unsigned int get_number_choices();

protected:

	/** Left text */
	std::string _left_text;

	/** Right text */
	std::string _right_text;

	/** Right sprite coords */
	int _x2, _y2;

	int _left_size_height, _left_size_width, _right_size_height, _right_size_width;
};

#endif
