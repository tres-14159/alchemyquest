// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : ChoiceItem.h
//  Description : 
//  Author      : Dennis Payne <dulsi@identicalsoftware.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#ifndef _CHOICE_ITEM_H_
#define _CHOICE_ITEM_H_

#include "MenuItem.h"

/**
* Generic (abstract) Choice Item class
*/
class ChoiceItem : public MenuItem {

public:

	/** Constructor */
	ChoiceItem();	

	/** Destructor */
	virtual ~ChoiceItem();	

	/** Set current choice */
	void set_current_choice(unsigned int choice);

	/** Return current choice */
	unsigned int get_current_choice(){ return _selection; }
	
	virtual unsigned int get_number_choices() = 0;

protected:

	/** Current selection */
	unsigned int _selection;

};



#endif
