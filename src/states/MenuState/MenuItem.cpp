// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : MenuItem.cpp
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#include <iostream>
#include "MenuItem.h"
#include "../../memory.h"

/************************************************************************/
/* Constructor                                                          */
/************************************************************************/
MenuItem::MenuItem()
{
	_is_selected = false;
	_is_locked = false;
	_alpha = 1.0f;
	_action = ACTION_UNKNOWN;
}

/************************************************************************/
/* Destructor                                                          */
/************************************************************************/
MenuItem::~MenuItem()
{
}

/************************************************************************/
/* Set X                                                                */
/************************************************************************/
void MenuItem::set_x(int x)
{
	_x = x;
}

/************************************************************************/
/* Set Y                                                                */
/************************************************************************/
void MenuItem::set_y(int y)
{
	_y = y;
}

/************************************************************************/
/* Set alpha                                                            */
/************************************************************************/
void MenuItem::set_alpha(float alpha)
{
	_alpha = alpha;
}

/************************************************************************/
/* Set action                                                           */
/************************************************************************/
void MenuItem::set_action(const std::string &act)
{
	if (act == "stop_state")
		_action = ACTION_STOP_STATE;
	else if (act == "undo")
		_action = ACTION_UNDO;
	else if (act == "retry")
		_action = ACTION_RETRY;
	else if (act == "options")
		_action = ACTION_OPTIONS;
	else if (act == "change_skin")
		_action = ACTION_CHANGESKIN;
	else if (act == "giveup")
		_action = ACTION_GIVEUP;
	else if (act == "quit")
		_action = ACTION_QUIT;
	else if (act == "render")
		_action = ACTION_RENDER;
	else if (act == "fullscreen")
		_action = ACTION_FULLSCREEN;
	else if (act == "framerate")
		_action = ACTION_FRAMERATE;
	else if (act == "colorblind")
		_action = ACTION_COLORBLIND;
	else if (act == "sound")
		_action = ACTION_SOUND;
	else if (act == "music")
		_action = ACTION_MUSIC;
	else
		_action = ACTION_UNKNOWN;
}

/************************************************************************/
/* Set selected                                                         */
/************************************************************************/
void MenuItem::set_selected(bool selected)
{
	_is_selected = selected;
}

/************************************************************************/
/* Set locked                                                           */
/************************************************************************/
void MenuItem::set_locked(bool locked)
{
	_is_locked = locked;
}
