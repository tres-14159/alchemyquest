// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : IniFile.cpp
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#include "IniFile.h"
#include "memory.h"
#include <iostream>

/************************************************************************/
/* Constructor                                                          */
/************************************************************************/
IniFile::IniFile()
{
}

/************************************************************************/
/* Destructor                                                           */
/************************************************************************/
IniFile::~IniFile()
{
	clear();
}

/************************************************************************/
/* Write a line                                                         */
/************************************************************************/
static void write_ln(FILE* p_file, std::string string)
{
#ifdef WIN32
	string += "\r\n";
#else
	string += "\n";
#endif
	fwrite(string.c_str(), 1, string.length(), p_file);
}

/************************************************************************/
/* Read a line                                                          */
/************************************************************************/
static std::string read_ln(FILE* p_file)
{
	std::string s = "";
	char c;
	
	while ((1 == fread(&c, 1, 1, p_file)) && (c != '\n') && (c != '\r'))
	{
		s += c;
	}

	return s;
}

/************************************************************************/
/* Read                                                                 */
/************************************************************************/
void IniFile::read(FILE* p_file)
{
	clear();

	while(!feof(p_file))
	{
		// Get line
		std::string line = read_ln(p_file);

		// Parse line
		if(line.length() >1)
		{
			int separator = line.find(" = ", 0);
			if(separator)
			{
				IniElement* e = my_new IniElement();
				e -> name = line.substr(0, separator);
				e -> value = line.substr(separator + 3, line.length());
				_list.insert(_list.end(), e);
			}
		}

	}
}

/************************************************************************/
/* Write                                                                */
/************************************************************************/
void IniFile::write(FILE* p_file)
{
	std::string section = "[Preferences]";
	write_ln(p_file, section);
	std::list<IniElement*>::iterator it = _list.begin();
	while(it != _list.end())
	{
		IniElement* e = (IniElement*)*it;
		std::string line = e -> name + " = " + e -> value;
		write_ln(p_file, line);
		it++;
	}
}

/************************************************************************/
/* Clear                                                                */
/************************************************************************/
void IniFile::clear()
{
	std::list<IniElement*>::iterator it = _list.begin();
	while(!_list.empty())
	{
		IniElement* e = (IniElement*) *it;
		my_delete(e);
		it = _list.erase(it);
	}
}

/************************************************************************/
/* Add                                                                  */
/************************************************************************/
void IniFile::add(std::string name, std::string value)
{
	std::list<IniElement*>::iterator it = _list.begin();
	while(it != _list.end())
	{
		IniElement* e = (IniElement*)*it;
		if(e -> name == name)
		{
			e -> value = value;
			return;
		}
		it++;
	}

	IniElement* e = my_new IniElement();
	e -> name = name;
	e -> value = value;
	_list.insert(_list.end(), e);
}

/************************************************************************/
/* Add                                                                  */
/************************************************************************/
void IniFile::add(std::string name, bool value)
{
	std::list<IniElement*>::iterator it = _list.begin();
	while(it != _list.end())
	{
		IniElement* e = (IniElement*)*it;
		if(e -> name == name)
		{
			if(value)
				e -> value = "True";
			else
				e -> value = "False";
			return;
		}
		it++;
	}

	IniElement* e = my_new IniElement();
	e -> name = name;

	if(value)
		e -> value = "True";
	else
		e -> value = "False";

	_list.insert(_list.end(), e);
}

/************************************************************************/
/* Add                                                                  */
/************************************************************************/
void IniFile::add(std::string name, int value)
{
	char tmp[30];
	sprintf(tmp, "%d", value);
	std::list<IniElement*>::iterator it = _list.begin();
	while(it != _list.end())
	{
		IniElement* e = (IniElement*)*it;
		if(e -> name == name)
		{
			e->value = tmp;
		}
		it++;
	}

	IniElement* e = my_new IniElement();
	e -> name = name;
	e->value = tmp;

	_list.insert(_list.end(), e);
}

/************************************************************************/
/* Get                                                                  */
/************************************************************************/
std::string IniFile::get(std::string name, std::string default_value)
{
	std::list<IniElement*>::iterator it = _list.begin();
	while(it != _list.end())
	{
		IniElement* e = (IniElement*)*it;
		if(e -> name == name)
		{
			return e -> value;
		}
		it++;
	}
	return default_value;
}

/************************************************************************/
/* Get                                                                  */
/************************************************************************/
bool IniFile::get(std::string name, bool default_value)
{
	std::list<IniElement*>::iterator it = _list.begin();
	while(it != _list.end())
	{
		IniElement* e = (IniElement*)*it;
		if(e -> name == name)
		{
			if (e->value == std::string("True"))
				return true;
			else
				return false;
		}
		it++;
	}
	return default_value;
}

/************************************************************************/
/* Get                                                                  */
/************************************************************************/
int IniFile::get(std::string name, int default_value)
{
	std::list<IniElement*>::iterator it = _list.begin();
	while(it != _list.end())
	{
		IniElement* e = (IniElement*)*it;
		if(e -> name == name)
		{
			return atol(e->value.c_str());
		}
		it++;
	}
	return default_value;
}
