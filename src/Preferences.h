// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : Preferences.h
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#ifndef _PREFERENCES_H_
#define _PREFREENCES_H_

#include <string>

/** Implements the preferences that users can save */
class Preferences
{
public:

	enum RenderTarget
	{
		SOFTWARE,
		HARDWARE
	};

	/** Render used */
	RenderTarget render_target;

	/** Fps Limit */
	int maxfps;

	/** Sound level */
	int sound_level;

	/** Music level */
	int music_level;

	/** Saves if the game is in fullscreen mode or not */
	bool fullscreen;
	
	int scale;

	/** Saves if user wants to use blind-mode */
	bool colorblind;

	/** Saves the last skin used */
	std::string skin;

	/** Constructor */
	Preferences();

	/** Read preferences from INI file */
	void read();

	/** Write preferences into INI file */
	void write();

private:

	/** Read INI file */
	void _read_options_file(FILE* p_file);

	/** Write INI file */
	void _write_options_file(FILE* p_file);

	/** Set default values */
	void _set_default();

};

/** Implementing a singleton instance of Preference */
Preferences* pref_get_instance();

#endif
