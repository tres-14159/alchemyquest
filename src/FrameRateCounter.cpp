// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : FrameRateCounter.cpp
//  Description : 
//  Author      : rombust (from the ClanLib team)
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#include "FrameRateCounter.h"

#include <SDL.h>

CL_FramerateCounter::CL_FramerateCounter(): impl(new CL_FramerateCounter_Generic)
{
	impl->start_time = SDL_GetTicks();
	impl->total_time = 0;
	impl->frame_time = 0;
	impl->fps_result = 0;
	impl->fps_counter = 0;
}

CL_FramerateCounter::~CL_FramerateCounter ()
{
	delete impl;
}

int CL_FramerateCounter::get_fps()
{
	return impl->fps_result;
}

void CL_FramerateCounter::set_fps_limit(int fps)
{
	if (fps > 0)
		impl->frame_time = 1000 / fps;
	else
		impl->frame_time = 0;
}

void CL_FramerateCounter::keep_alive()
{
	int cur_time = SDL_GetTicks();
	int delta_time = cur_time - impl->start_time;
	impl->start_time = cur_time;

	if (delta_time < impl->frame_time)
	{
		SDL_Delay(impl->frame_time - delta_time);
		impl->total_time += impl->frame_time;
	}
	else
	{
		impl->total_time += delta_time;
	}

	if(impl->total_time >= 1000)	// One second has passed
	{
		impl->fps_result = impl->fps_counter + 1;
		impl->fps_counter = impl->total_time = 0;
	}
	impl->fps_counter++;	// Increase fps
}
