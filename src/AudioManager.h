// **********************************************************************
//                            OpenAlchemist
//                        ---------------------
//
//  File        : AudioManager.h
//  Description : 
//  Author      : Guillaume Delhumeau <guillaume.delhumeau@gmail.com>
//  License     : GNU General Public License 2 or higher
//
// **********************************************************************

#ifndef _MUSIC_MANAGER_H_
#define _MUSIC_MANAGER_H_

#include <SDL.h>
#include <SDL_mixer.h>

enum SOUNDS{

	SOUND_MOVE = 0,
	SOUND_FALL,
	SOUND_DESTROY,
	SOUND_CREATION,

	NB_SOUNDS
};

/** Audio manager */
class AudioManager{

public:

	/** Constructor */
	AudioManager();

	/** Destructor */
	~AudioManager();

	/** Init */
	void init();

	/** Term */
	void term();

	/** Set music volume */
	void set_music_volume(int volume);

	/** Set sounds volume */
	void set_sounds_volume(int volume);

	/** Play a sound */
	void play_sound(int sound_index);
	
	/** Pause sounds */
	void pause_sounds();

	/** Resume sounds */
	void resume_sounds();

private:

	/** vorbis */
	Mix_Music *_music_obj;

	/** Sounds */
	Mix_Chunk *_sounds_p[NB_SOUNDS];
};

/** Audio Manager instance */
extern AudioManager g_audio_manager;

#endif
